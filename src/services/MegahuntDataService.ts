import {
  FilterState,
  SearchResult,
  EntityPart,
  Tag,
  Type,
  Collection,
  DataServiceId,
  GetResultResponse
} from 'src/models/types';
import DataService from './DataService';
import qs from 'querystring';
import axios, { AxiosInstance } from 'axios';
import os from 'os';

export default class MegahuntDataService implements DataService {
  serviceId: DataServiceId = DataServiceId.MEGAHUNT_DATA_SERVICE_ID;
  host = 'https://pictureshop.shotgunstudio.com';
  client_secret = 'nlcxkofudceeuqcvkfoi5r!qX';
  client_id = 'hunter';
  project_id = 583;

  // host = 'https://megahunt.shotgunstudio.com';
  // client_secret = 'kekvwvzafcfdseqQsdzn5#xud';
  // client_id = 'hunter';

  axiosInstance: AxiosInstance;

  accessToken = '';
  refreshToken = '';

  constructor() {
    this.axiosInstance = this.createAxiosInstance();
  }

  createAxiosInstance() {
    const axiosInstance = axios.create({
      baseURL: this.host,
      timeout: 0,
      params: {},
      headers: {
        'Content-Type': 'application/json'
      }
    });

    axiosInstance.interceptors.request.use(async config => {
      await this.getAccessToken();
      config.headers.common['Authorization'] = `Bearer ${this.accessToken}`;
      return config;
    });

    axiosInstance.interceptors.response.use(undefined, async error => {
      const originalRequest = error.config;
      if (
        error.response &&
        error.response.status === 401 &&
        !originalRequest._retry
      ) {
        originalRequest._retry = true;
        await this.getAccessToken();
        originalRequest.headers.Authorization = `Bearer ${this.accessToken}`;
        return axios(originalRequest);
      }
      return Promise.reject(error);
    });

    return axiosInstance;
  }

  /**
   * Returns list of tags from shotgun database
   */
  async getTags(): Promise<Tag[]> {
    const tags: Tag[] = [];

    // Collects all tags
    // const apiResp = await this.requestEntityList('tags', {
    //   fields: ['name'],
    //   filters: [],
    //   sort: ['name'],
    //   page: {
    //     size: 1000,
    //     number: 1
    //   }
    // });
    // (apiResp.data as any[]).forEach((resp: any) => {
    //   if (resp.attributes && resp.attributes.name) {
    //     tags.push({
    //       id: resp.id,
    //       name: resp.attributes.name,
    //     });
    //   }
    // });

    // Collects only tags used by assets in project
    const apiResp = await this.requestEntitySummary('assets', {
      filters: [['project', 'is', { type: 'Project', id: this.project_id }]],
      summary_fields: [
        {
          field: 'id',
          type: 'count'
        }
      ],
      grouping: [{ field: 'tags', type: 'exact', direction: 'asc' }]
    });

    (apiResp.data.groups as any[]).forEach((resp: any) => {
      if (resp.group_value && resp.group_value.length >= 1) {
        tags.push({
          id: resp.group_value[0].id,
          name: resp.group_value[0].name
        });
      }
    });

    return tags;
  }

  async getTypes(): Promise<Type[]> {
    const data: Type[] = [];

    // const apiResp = await this.requestEntitySchema('Asset', 'sg_asset_type');
    // (apiResp.data.properties.valid_values.value as string[]).sort(function(
    //   a,
    //   b
    // ) {
    //   return a.toLowerCase().localeCompare(b.toLowerCase());
    // });
    // (apiResp.data.properties.valid_values.value as any[]).forEach(
    //   (resp: any) => {
    //     data.push({
    //       id: resp,
    //       name: resp
    //     });
    //   }
    // );

    // Collects only types used by assets in project
    const apiResp = await this.requestEntitySummary('assets', {
      filters: [['project', 'is', { type: 'Project', id: this.project_id }]],
      summary_fields: [
        {
          field: 'id',
          type: 'count'
        }
      ],
      grouping: [{ field: 'sg_asset_type', type: 'exact', direction: 'asc' }]
    });

    (apiResp.data.groups as any[]).forEach((resp: any) => {
      if (resp.group_value && resp.group_value.length >= 1) {
        console.log(resp);
        data.push({
          id: resp.group_value,
          name: resp.group_value
        });
      }
    });

    return data;
  }

  async getCollections(): Promise<Collection[]> {
    const data: Collection[] = [];

    const apiResp = await this.requestEntityList('CustomEntity07', {
      fields: ['code', 'id'],
      filters: [],
      sort: ['code'],
      page: {
        size: 2000,
        number: 1
      }
    });

    (apiResp.data as any[]).forEach((resp: any) => {
      if (resp.attributes && resp.attributes.code) {
        data.push({
          id: resp.id,
          name: resp.attributes.code
        });
      }
    });
    return data;
  }

  async getResults(filter: FilterState): Promise<GetResultResponse> {
    const data: SearchResult[] = [];
    const startTime = performance.now();

    // query defaults
    const query = {
      fields: [
        'code',
        'description',
        'user',
        'user.HumanUser.image',
        'updated_at',
        'tags',
        'sg_collections',
        'sg_asset_type',
        'image',
        'sg_virtual_name',
        'sg_filmstrip_image',
        'sg_movie',
        'sg_favorites'
      ],
      filters: {
        logical_operator: 'and',
        conditions: [['project.Project.id', 'is', this.project_id]]
      },
      sort: ['-code'],
      page: {
        size: filter.pageSize,
        number: filter.page || 1
      }
    };

    // Query Sorting
    if (filter.sorting) {
      query.sort = [filter.sorting.value];
    }

    // Filter: Types
    if (filter.type) {
      query.filters.conditions.push(['sg_asset_type', 'is', filter.type.name]);
    }

    // Filter: Tags
    if (filter.tag) {
      filter.tag.map((item: any, index) => {
        query.filters.conditions.push([
          'tags',
          'is',
          { type: 'Tag', id: item.id }
        ] as any[]);
      });
    }

    // Filter: Collections
    if (filter.collection && filter.collection.length >= 1) {
      const collections: any[] = [];
      filter.collection.map((item, index) => {
        collections.push({ type: 'CustomEntity07', id: item.id });
        // query.filters.conditions.push(['sg_collections', 'is', {'type': 'CustomEntity07','id': item.id}])
      });
      query.filters.conditions.push([
        'sg_collections',
        'in',
        collections
      ] as any[]);
    }

    // Filter: Favorites
    if (filter.showFavorites) {
      // console.log(`%c USERNAME: ${userInfo.username}`, 'color: DodgerBlue');
      const shotgunUser = await this.requestCurrentUser();
      if (shotgunUser && shotgunUser.id) {
        query.filters.conditions.push([
          'sg_favorites.HumanUser.id',
          'is',
          shotgunUser.id
        ]);
      }
      // query.filters.conditions.push({
      //   'logical_operator': 'or',
      //   'conditions': [
      //     ['sg_favorites.HumanUser.login', 'contains', userInfo.username],
      //     ['sg_favorites.HumanUser.email', 'contains', userInfo.username],
      //     ['sg_favorites.HumanUser.name', 'contains', userInfo.username]
      //   ]
      // })
    }

    // Filter: Search - Name, Description and Tags
    if (filter.searchQuery) {
      const parts = filter.searchQuery.split(' ');
      parts.map((item, index) => {
        query.filters.conditions.push({
          logical_operator: 'or',
          conditions: [
            ['code', 'contains', item],
            ['description', 'contains', item],
            ['tags.Tag.name', 'contains', item]
          ]
        } as any);
      });
    }

    // Filter: Shopping cart
    if (filter.showShoppingCart) {
      if (filter.shoppingCart.length) {
        const ids = filter.shoppingCart.map(item => {
          return parseInt(item);
        });
        query.filters.conditions.push(['id', 'in', ids as any]);
      } else {
        query.filters.conditions.push(['id', 'is', -1]);
      }
    }

    // Request Summary
    const sumResp = await this.requestEntitySummary('assets', {
      filters: query.filters,
      summary_fields: [
        {
          field: 'id',
          type: 'count'
        }
      ]
    });

    // Request Query
    // console.log(`%c ${JSON.stringify(query, null, 3)}`, 'color: DodgerBlue');
    const apiResp = await this.requestEntityList('assets', { ...query });
    // console.log(apiResp)

    (apiResp.data as any[]).forEach((resp: any) => {
      if (resp.attributes && resp.type) {
        data.push({
          id: (resp.id as string).toString(),
          name: resp.attributes.sg_virtual_name,
          url: `${
            this.host
          }/detail/${resp.type as string}/${resp.id as string}`,
          description: resp.attributes.description,
          type: resp.attributes.sg_asset_type,
          thumbnail: resp.attributes.image,
          tags: (resp.relationships.tags.data as any[]).map(obj => {
            return obj.name;
          }),
          collections: (resp.relationships.sg_collections.data as any[]).map(
            obj => {
              return obj.name;
            }
          ),
          filmstrip: !resp.attributes.sg_filmstrip_image
            ? ''
            : resp.attributes.sg_filmstrip_image.url,
          video: !resp.attributes.sg_movie ? '' : resp.attributes.sg_movie.url,
          isFavorited: false
        });
      }
    });

    const elapsedTime = ((performance.now() - startTime) * 0.001).toFixed(2);
    console.log(
      `%c ${data.length}/${sumResp.data.summaries
        .id as string} results (${elapsedTime} seconds)`,
      'color: DodgerBlue'
    );

    return {
      data,
      totalItems: sumResp.data.summaries.id
    };
  }

  async getParts(entityId: string): Promise<any[]> {
    // await new Promise(resolve => setTimeout(resolve, 1000));
    const data: EntityPart[] = [];
    const startTime = performance.now();

    // query defaults
    const query = {
      fields: [
        'id',
        'code',
        'description',
        'sg_path_to_frames',
        'image',
        'type'
      ],
      filters: {
        logical_operator: 'and',
        conditions: [
          ['project.Project.id', 'is', this.project_id],
          ['entity.Asset.id', 'is', entityId]
        ]
      },
      sort: ['-code'],
      page: {
        size: 100,
        number: 1
      }
    };

    // Request Query
    // console.log(`%c ${JSON.stringify(query, null, 3)}`, 'color: DodgerBlue');
    const apiResp = await this.requestEntityList('versions', { ...query });
    // console.log(apiResp)

    (apiResp.data as any[]).forEach((resp: any) => {
      if (resp.attributes && resp.type) {
        data.push({
          id: (resp.id as string).toString(),
          name: resp.attributes.code,
          url: `${
            this.host
          }/detail/${resp.type as string}/${resp.id as string}`,
          description: resp.attributes.description,
          type: resp.attributes.sg_asset_type,
          thumbnail: resp.attributes.image
        });
      }
    });

    const elapsedTime = ((performance.now() - startTime) * 0.001).toFixed(2);
    console.log(
      `%c ${data.length} Parts (${elapsedTime} seconds)`,
      'color: DodgerBlue'
    );

    return data;
  }

  /**
   * Directly toggle the entities property 'favorite' and then send updated
   * data to database. We don't trigger a requery to database in order to avoid
   * wait time
   * @param entity
   */
  async toggleFavorite(entity: SearchResult): Promise<any> {
    console.log('Toggled Favorite', entity);
    await new Promise(resolve => setTimeout(resolve, 1000));
    return {
      success: true
    };
  }

  /**
   * Gets the token for the root account
   */
  async getAccessToken(): Promise<boolean> {
    if (!this.client_id || !this.client_secret) {
      console.error(
        'Shotgun > getAccessToken: client_id or client_secret is not supplied.'
      );
      return false;
    }

    // prepare the query string
    const queryString = qs.stringify({
      grant_type: 'client_credentials',
      client_id: this.client_id,
      client_secret: this.client_secret
    });

    // fire the axios call
    const url = `${this.host}/api/v1/auth/access_token`;
    const resp = await axios.post(url, queryString, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json'
      }
    });

    this.accessToken = resp.data.access_token;
    this.refreshToken = resp.data.refresh_token;
    return true;
  }

  async requestEntityList(entityType: string | null = null, query: any = null) {
    let content_type = 'application/vnd+shotgun.api3_array+json';
    if (query && !Array.isArray(query.filters)) {
      content_type = 'application/vnd+shotgun.api3_hash+json';
    }

    // https://yoursite.shotgunstudio.com/api/v1/entity/{entity}/_search'
    const url = `/api/v1/entity/${entityType as string}/_search`;
    const resp = await this.axiosInstance.post(url, JSON.stringify(query), {
      headers: {
        Accept: 'application/json',
        'Content-Type': content_type
      }
    });
    return resp.data;
  }

  async requestEntitySchema(
    entityType: string | null = null,
    field: string | null = null
  ) {
    let url = `/api/v1/schema/${entityType as string}/fields`;
    if (field) {
      url += `/${field}`;
    }
    const resp = await this.axiosInstance.get(url, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/vnd+shotgun.api3_array+json'
      }
    });
    return resp.data;
  }

  async requestCurrentUser() {
    const userInfo = os.userInfo();
    const resp = await this.requestEntityList('HumanUser', {
      fields: ['name', 'id', 'login'],
      filters: {
        logical_operator: 'or',
        conditions: [
          ['login', 'contains', userInfo.username],
          ['email', 'contains', userInfo.username],
          ['name', 'contains', userInfo.username]
        ]
      },
      page: {
        size: 1,
        number: 1
      }
    });
    if (resp && Array.isArray(resp.data)) {
      return resp.data[0];
    }
    return null;
  }

  async requestEntitySummary(
    entityType: string | null = null,
    query: any = null
  ) {
    let content_type = 'application/vnd+shotgun.api3_array+json';
    if (query && !Array.isArray(query.filters)) {
      content_type = 'application/vnd+shotgun.api3_hash+json';
    }

    // https://yoursite.shotgunstudio.com/api/v1/entity/{entity}/_summarize
    const url = `/api/v1/entity/${entityType as string}/_summarize`;
    const resp = await this.axiosInstance.post(url, JSON.stringify(query), {
      headers: {
        Accept: 'application/json',
        'Content-Type': content_type
      }
    });
    return resp.data;
  }
}
