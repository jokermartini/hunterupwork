import { FilterState, SearchResult, Tag, Type, Collection, DataServiceId, GetResultResponse } from 'src/models/types';
import DataService from './DataService';

export default class IStockService implements DataService {
  serviceId: DataServiceId = DataServiceId.ISTOCK_DATA_SERVICE_ID;

  constructor () {
    // pass
  }

  async getTags (): Promise<Tag[]> {
    const tags: Tag[] = [
      { id: '1', name: 'IStock Tag1' },
      { id: '2', name: 'IStock Tag2' },
    ];

    await new Promise(resolve => setTimeout(resolve, 1000));
    return tags;
  }

  async getTypes (): Promise<Type[]> {
    const data: Type[] = [
      { id: '1', name: 'IStock Type1' },
      { id: '2', name: 'IStock Type2' },
    ];

    await new Promise(resolve => setTimeout(resolve, 1000));
    return data;
  }

  async getCollections (): Promise<Collection[]> {
    const data: Type[] = [
      { id: '1', name: 'IStock Tag1' },
      { id: '2', name: 'IStock Tag2' },
    ];

    await new Promise(resolve => setTimeout(resolve, 1000));
    return data;
  }

  async getResults (filter: FilterState): Promise<GetResultResponse> {
    const data: SearchResult[] = [
      { 
        id: '1',
        url: 'https://google.com',
        name: 'Rocks 01',
        thumbnail: 'https://picsum.photos/360/180',
      },
      { 
        id: '2', 
        url: 'https://google.com',
        name: 'Rocks 02',
        thumbnail: 'https://picsum.photos/360/180',
      },
      { 
        id: '3',
        url: 'https://google.com',
        name: 'Rocks 03',
        thumbnail: 'https://picsum.photos/360/180',
      },
      { 
        id: '4', 
        url: 'https://google.com',
        name: 'Rocks 04',
        thumbnail: 'https://picsum.photos/360/180',
      },
      { 
        id: '5',
        url: 'https://google.com',
        name: 'Rocks 05',
        thumbnail: 'https://picsum.photos/360/180',
      },
      { 
        id: '6', 
        url: 'https://google.com',
        name: 'Rocks 06',
        thumbnail: 'https://picsum.photos/360/180',
      },
    ];

    // use filter to search data from api
    filter

    await new Promise(resolve => setTimeout(resolve, 1000));
    return {
      data,
      totalItems: data.length,
    };
  }

  async toggleFavorite (entity: SearchResult): Promise<any> {
    await new Promise(resolve => setTimeout(resolve, 1000));
    return {
      success: true,
    };
  }

  async getParts (entityId: string): Promise<any[]> {
    await new Promise(resolve => setTimeout(resolve, 1000));
    return [];
  }
}
