import { FilterState, SearchResult, Tag, Type, Collection, DataServiceId, GetResultResponse } from 'src/models/types';
import DataService from './DataService';

export default class GettyDataService implements DataService {
  serviceId: DataServiceId = DataServiceId.GETTY_DATA_SERVICE_ID;

  constructor () {
    // pass
  }

  async getTags (): Promise<Tag[]> {
    const tags: Tag[] = [
      { id: '1', name: 'Getty Tag1' },
      { id: '2', name: 'Getty Tag2' },
    ];

    await new Promise(resolve => setTimeout(resolve, 1000));
    return tags;
  }

  async getTypes (): Promise<Type[]> {
    const data: Type[] = [
      { id: '1', name: 'Getty Type1' },
      { id: '2', name: 'Getty Type2' },
    ];

    await new Promise(resolve => setTimeout(resolve, 1000));
    return data;
  }

  async getCollections (): Promise<Collection[]> {
    const data: Type[] = [
      { id: '1', name: 'Getty Tag1' },
      { id: '2', name: 'Getty Tag2' },
    ];

    await new Promise(resolve => setTimeout(resolve, 1000));
    return data;
  }

  async getResults (filter: FilterState): Promise<GetResultResponse> {
    const data: SearchResult[] = [
      { 
        id: '1062',
        url: 'https://picsum.photos/id/1062',
        name: 'Pug 01',
        thumbnail: 'https://picsum.photos/id/1062/640/360',
        description: 'Here is some random metadata that will be displayed for the users to read if the need to know more about the item.',
      },
      { 
        id: '1063', 
        url: 'https://picsum.photos/id/1063',
        name: 'Tunnel 02',
        thumbnail: 'https://picsum.photos/id/1063/640/360',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc convallis nisi enim, vitae interdum felis lacinia quis. Suspendisse potenti. Pellentesque vitae nunc placerat, viverra diam a, eleifend nisi. Donec nulla urna, semper sed mollis id, porta quis mauris. Suspendisse posuere venenatis libero, eu mattis neque fringilla a. Donec consequat eget.',
      },
      { 
        id: '1064',
        url: 'https://picsum.photos/id/1064',
        name: 'Clouds 03',
        thumbnail: 'https://picsum.photos/id/1064/640/360',
      },
      { 
        id: '1065', 
        url: 'https://picsum.photos/id/1065',
        name: 'Passage 04',
        thumbnail: 'https://picsum.photos/id/1065/640/360',
      },
      { 
        id: '1066',
        url: 'https://picsum.photos/id/1066',
        name: 'Newborn Baby with Dad Playing Guitar 05',
        thumbnail: 'https://picsum.photos/id/1066/640/360',
      },
      { 
        id: '1067', 
        url: 'https://picsum.photos/id/1067',
        name: 'Cityscape 06',
        thumbnail: 'https://picsum.photos/id/1067/640/360',
      },
      { 
        id: '1068', 
        url: 'https://picsum.photos/id/1068',
        name: 'Table 07',
        thumbnail: 'https://picsum.photos/id/1068/640/360',
      },
    ];
    
    // let i;
    // const count = 20;
    // const start = 50
    // const end = start + count;
    // for (i = start; i < end; i++) {
    //   const obj: SearchResult = { 
    //     id: i.toString(), 
    //     url: `https://picsum.photos/id/${i.toString()}`,
    //     name: `Title ${i.toString()}`,
    //     thumbnail: `https://picsum.photos/id/${i.toString()}/640/360`,
    //     description:'Description here...'
    //    };
    //   data.push(obj)
    // }

    // use filter to search data from api
    filter

    await new Promise(resolve => setTimeout(resolve, 1000));
    return {
      data,
      totalItems: data.length,
    };
  }

  async toggleFavorite (entity: SearchResult): Promise<any> {
    await new Promise(resolve => setTimeout(resolve, 1000));
    return {
      success: true,
    };
  }

  async getParts (entityId: string): Promise<any[]> {
    await new Promise(resolve => setTimeout(resolve, 1000));
    return [];
  }
}
