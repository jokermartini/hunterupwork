import { FilterState, SearchResult, Tag, Type, Collection, GetResultResponse } from 'src/models/types';

export default interface DataService {
  serviceId: string;

  getTags: () => Promise<Tag[]>,
  getTypes: () => Promise<Type[]>,
  getCollections: () => Promise<Collection[]>,

  getResults: (filter: FilterState) => Promise<GetResultResponse>,
  toggleFavorite: (entity: SearchResult) => Promise<any>,
  getParts: (entityId: string) => Promise<any>,
}
