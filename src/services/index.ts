import { DataServiceId } from 'src/models/types';
import DataService from './DataService';
import GettyDataService from './GettyDataService';
import IStockDataService from './IStockDataService';
import MegahuntDataService from './MegahuntDataService';

const SERVICES: {[key: string]: DataService} = {
  [DataServiceId.GETTY_DATA_SERVICE_ID]: new GettyDataService(),
  [DataServiceId.ISTOCK_DATA_SERVICE_ID]: new IStockDataService(),
  [DataServiceId.MEGAHUNT_DATA_SERVICE_ID]: new MegahuntDataService(),
}

export function getService (serviceId: DataServiceId): DataService {
  return SERVICES[serviceId]
}