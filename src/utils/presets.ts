/* eslint-disable */

import { BrowserWindow } from "electron";
import { FilterPreset } from "src/models/types";

const fs = require('fs')
const path = require('path')
const { remote } = require('electron')

function getPresetsDirectory() {
  const dir = path.join(
    remote.app.getPath('userData'),
    'Hunter',
    'Presets'
  );
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, {
      recursive: true
    });
  }
  return dir;
}

export function saveFilterPreset(preset: FilterPreset) {
  const dir = getPresetsDirectory();
  let options = {
    defaultPath: dir,
    filters: [
      { name: 'All Files', extensions: ['*'] },
      { name: 'Filter Preset', extensions: ['hfp'] },
    ]
  };

  const mainWindow = remote.getCurrentWindow()
  let filepath = remote.dialog.showSaveDialogSync(mainWindow, options);
  if (!filepath) {
    return false;
  }

  let obj = preset
  try {
    let str = JSON.stringify(obj, null, 2);
    fs.writeFileSync(filepath, str, 'utf-8');
  } catch (err) {
    throw err
  }
}

export function loadFilterPreset(): FilterPreset {
  const dir = getPresetsDirectory();
  const mainWindow = remote.getCurrentWindow()
  let filepaths = remote.dialog.showOpenDialogSync(mainWindow, {
      defaultPath: dir,
      filters: [
        { name: 'All Files', extensions: ['*'] },
        { name: 'Filter Preset', extensions: ['hfp'] },
      ],
      properties: ['openFile']
    });

  if (!filepaths) {
    return {filters: {}};
  }
  
  let filepath = filepaths[0];
  try {
    const content = fs.readFileSync(filepath, 'utf8');
    const obj = JSON.parse(content);
    return obj
  } catch (err) {
    throw err
  }
}