export interface Sorting {
  label: string;
  value: string;
}

export interface Database {
  id: DataServiceId,
  label: string;
}

export interface Tag {
  id: string;
  name: string;
}

export interface Type {
  id: string;
  name: string;
}

export interface Collection {
  id: string;
  name: string;
}

export interface SearchResult {
  id: string;
  url: string;
  name: string;
  thumbnail: string;
  isSelected?: boolean;
  description?: string;
  type?: string;
  tags?: string[];
  collections?: string[];
  filmstrip?: string;
  video?: string;
  isFavorited?: boolean;
}

export interface GetResultResponse {
  data: SearchResult[],
  totalItems: number;
}

export interface EntityPart {
  id: string;
  url: string;
  name: string;
  thumbnail: string;
  description?: string;
  type: any;
}

export interface AppSettings {
  mediaThumbnailRatio?: number;
  mediaThumbnailSize?: number;
  mediaPlaybackSpeed?: number;
  mediaAutoPlay?: boolean;
};

export enum DataServiceId {
  GETTY_DATA_SERVICE_ID = 'getty',
  ISTOCK_DATA_SERVICE_ID = 'istock',
  MEGAHUNT_DATA_SERVICE_ID = 'megahunt',
}

export interface FilterState {
  sorting?: Sorting | null;
  tag?: Tag[] | null;
  type?: Type | null;
  collection?: Collection[] | null;
  searchQuery?: string;
  showFavorites?: boolean | null;
  showShoppingCart?: boolean | null;
  shoppingCart: string[];

  pageSize: number;
  page: number;
};

export interface FilterDataState {
  sortings: Sorting[];
  tags: Tag[];
  types: Type[];
  collections: Collection[];
}

export interface AppState {
  settings: AppSettings;
  database: Database;
  filters: FilterState;
  filterData: FilterDataState,
  loadingData: boolean;
};

export interface ResultState {
  results: SearchResult[];
  loading: boolean;
  selected: string[];
  totalItems: number;
}

export interface FilterPreset {
  filters: {
    sorting?: Sorting | null;
    tag?: Tag | null;
    type?: Type | null;
    collection?: Collection | null;
  },
}
