import { ActionTree } from 'vuex';
import store, { StateInterface } from '../index';
import {
  AppState,
  Collection,
  Database,
  FilterState,
  Sorting,
  Tag,
  Type,
  AppSettings,
  FilterPreset,
  SearchResult
} from 'src/models/types';
import { getService } from 'src/services';
import * as presetSrvc from 'src/utils/presets';

const actions: ActionTree<AppState, StateInterface> = {
  async init({ commit, dispatch, state }) {
    commit('setFilters', {
      searchQuery: ''
    });
    dispatch('results/init', null, { root: true });

    commit('setFilterData', {
      tags: [],
      types: [],
      collections: []
    });

    dispatch('loadFilterData');
    // reload using current database
    await dispatch('setDatabase', state.database);
  },
  setSettings({ commit }, payload: Partial<AppSettings>) {
    commit('setSettings', payload);
  },
  resetFilters({ commit }) {
    commit('setFilters', {
      tag: null,
      type: null,
      collection: null
    });
  },
  setDatabase({ commit, state, dispatch }, payload: Database) {
    const oldDbId = state.database.id;
    // if db not changed
    if (oldDbId === payload.id) {
      return;
    }

    commit('setDatabase', payload);

    commit('setLoadingData', true);
    try {
      dispatch('loadFilterData');

      commit('initFilters');
    } catch (err) {
      console.log(err);
    }
    commit('setLoadingData', false);
  },
  async loadFilterData({ commit, state }) {
    const database = state.database;

    if (database && database.id) {
      try {
        const service = getService(database.id);
        const [tags, types, collections] = await Promise.all([
          service.getTags(),
          service.getTypes(),
          service.getCollections()
        ]);
        commit('setFilterData', { tags, types, collections });
      } catch (err) {
        console.log(err);
      }
    }
  },

  updateFilter({ commit }, payload: Partial<FilterState>) {
    commit('setFilters', payload);
  },
  updateFilterSorting({ commit }, payload: Sorting) {
    commit('setFilters', { sorting: payload });
  },
  updateFilterType({ commit }, payload: Type) {
    commit('setFilters', { type: payload });
  },
  updateFilterCollection({ commit }, payload: Collection[]) {
    commit('setFilters', { collection: payload });
  },
  updateFilterTag({ commit }, payload: Tag[]) {
    commit('setFilters', { tag: payload });
  },
  updateFilterSearchQuery({ commit }, payload: string) {
    commit('setFilters', { searchQuery: payload });
  },
  toggleShoppingCartItem({ commit, state }, payload: string) {
    const newSelected = [...(state.filters.shoppingCart || [])];
    if (newSelected.indexOf(payload) === -1) {
      newSelected.push(payload);
    } else {
      newSelected.splice(newSelected.indexOf(payload), 1);
    }

    commit('setFilters', {
      shoppingCart: newSelected
    });
  },
  clearShoppingCart({ commit, state }) {
    commit('setFilters', { shoppingCart: [], showShoppingCart: false });
  },
  toggleFavoriteItem({ commit, state }, payload: SearchResult) {
    const database = state.database;

    if (database && database.id) {
      try {
        const service = getService(database.id);
        service.toggleFavorite(payload);
      } catch (err) {
        console.log(err);
      }
    }
  },

  setLoadingData({ commit }, payload: boolean) {
    commit('setLoadingData', payload);
  },

  // filter actions
  saveFiltersPreset({ state }) {
    presetSrvc.saveFilterPreset({
      filters: {
        tag: state.filters.tag as any,
        type: state.filters.type,
        collection: state.filters.collection as any,
        sorting: state.filters.sorting
      }
    });
  },
  loadFilterPreset({ commit }) {
    const preset: FilterPreset = presetSrvc.loadFilterPreset();
    commit('setFilters', {
      tag: preset.filters.tag,
      type: preset.filters.type,
      collection: preset.filters.collection,
      sorting: preset.filters.sorting
    });
  }
};

export default actions;
