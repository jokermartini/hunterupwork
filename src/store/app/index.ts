import { Module } from 'vuex';
import { StateInterface } from '../index';
import state from './state';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import { AppState } from 'src/models/types';

const exampleModule: Module<AppState, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
};

export default exampleModule;
