import { AppState, Database, FilterDataState, FilterState, AppSettings } from 'src/models/types';
import { MutationTree } from 'vuex';

const mutation: MutationTree<AppState> = {

  setSettings (state, payload: Partial<AppSettings>) {
    state.settings = {
      ...state.settings,
      ...payload
    };
  },
  setLoadingData (state, payload: boolean) {
    state.loadingData = payload;
  },
  setDatabase (state, payload: Database) {
    state.database = payload;
  },
  setFilterData (state, payload: Partial<FilterDataState>): void {
    state.filterData = {
      ...state.filterData,
      ...payload,
    };
  },
  initFilters (state) {
    state.filters = {
      sorting: state.filters.sorting,
      tag: null,
      type: null,
      collection: null,
      searchQuery: '',
      showFavorites: false,
      shoppingCart: [],

      pageSize: 10,
      page: 1,
    };
  },
  setFilters (state, payload: Partial<FilterState>): void {
    state.filters = {
      ...state.filters,
      ...payload,
    }
  }
};

export default mutation;
