import { Sorting, Database, AppState, DataServiceId } from 'src/models/types';

export const defaultSortings: Sorting[] = [
  { label: 'Created Ascending', value: '-created_at' },
  { label: 'Created Descending', value: 'created_at' },
  { label: 'Modified Ascending', value: '-updated_at' },
  { label: 'Modified Descending', value: 'updated_at' },
  { label: 'Name Ascending', value: 'code' },
  { label: 'Name Descending', value: '-code' }
];

export const defaultDatabases: Database[] = [
  {
    id: DataServiceId.MEGAHUNT_DATA_SERVICE_ID,
    label: 'LA Asset Library'
  },
  {
    id: DataServiceId.GETTY_DATA_SERVICE_ID,
    label: 'Getty Images'
  },
  {
    id: DataServiceId.ISTOCK_DATA_SERVICE_ID,
    label: 'Istock'
  }
];

const state: AppState = {
  settings: {
    mediaThumbnailRatio: 1.7778,
    mediaThumbnailSize: 320,
    mediaPlaybackSpeed: 18,
    mediaAutoPlay: true
  },
  database: defaultDatabases[0],
  filters: {
    sorting: defaultSortings[2],
    tag: null,
    type: null,
    collection: null,
    searchQuery: '',
    showFavorites: null,
    showShoppingCart: null,
    shoppingCart: [],

    pageSize: 10,
    page: 1,
  },
  filterData: {
    sortings: defaultSortings,
    tags: [],
    types: [],
    collections: []
  },
  loadingData: false
};

export default state;
