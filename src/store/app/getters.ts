import { AppState } from 'src/models/types';
import { GetterTree } from 'vuex';
import { StateInterface } from '../index';

const getters: GetterTree<AppState, StateInterface> = {
  settings: (state) => state.settings,
  filters: (state) => state.filters,
  filterData: (state) => state.filterData,
  database: (state) => state.database,
  loadingData: (state) => state.loadingData,
};

export default getters;
