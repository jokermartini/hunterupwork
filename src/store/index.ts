/* eslint-disable @typescript-eslint/no-unsafe-call */
import { store } from 'quasar/wrappers';
import Vuex from 'vuex';

import app from './app';
import results from './results';
import { ResultState, AppState } from 'src/models/types';

// import example from './module-example';
// import { ExampleStateInterface } from './module-example/state';

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export interface StateInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  example: unknown;
  app: AppState;
  results: ResultState,
}

import { createPersistedState } from 'vuex-electron'

const vuexElectonPlugin = createPersistedState({
  // @john, mutation contains mutation name inside white list and blacklist
  // you can whitelist / blacklist specific mutations
  
  // whitelist: (mutation) => {
  //   return true
  // },
  // blacklist: (mutation) => {
  //   debugger
  //   return true
  // }
})

export default store(function ({ Vue }) {
  Vue.use(Vuex);

  const Store = new Vuex.Store<StateInterface>({
    modules: {
      // example
      app,
      results,
    },
    plugins: [vuexElectonPlugin],
    // enable strict mode (adds overhead!)
    // for dev mode only,
    strict: !!process.env.DEBUGGING
  });

  return Store;
});
