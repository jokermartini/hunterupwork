import { ResultState, SearchResult } from 'src/models/types';
import { MutationTree } from 'vuex';

const mutation: MutationTree<ResultState> = {
  setResults (state, payload: SearchResult[]) {
    state.results = payload;
  },
  setLoading (state, value: boolean) {
    state.loading = value;
  },
  setSelected (state, selected: string[]) {
  	state.selected = selected;
  },
  setTotalItems (state, totalItems: number) {
  	state.totalItems = totalItems;
  },
};

export default mutation;
