import { ResultState } from 'src/models/types';

const state: ResultState = {
  loading: false,
  results: [],
  selected: [],
  totalItems: 1,
};

export default state;
