import { ResultState } from 'src/models/types';
import { GetterTree } from 'vuex';
import state from '../app/state';
import { StateInterface } from '../index';

const getters: GetterTree<ResultState, StateInterface> = {
  loading: (state) => state.loading,
  results: (state) => state.results,
  selected: (state) => state.selected,
  totalItems: (state) => state.totalItems,
};

export default getters;
