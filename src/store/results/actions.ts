import { Database, FilterState, ResultState } from 'src/models/types';
import { getService } from 'src/services';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';

const actions: ActionTree<ResultState, StateInterface> = {
  init ({ commit }) {
    commit('setResults', [])
    commit('setSelected', [])
  },
  async loadResults ({ commit }, payload: { database: Database, filters: FilterState }) {
    commit('setLoading', true);
    try {
      const service = getService(payload.database.id)
      const results = await service.getResults(payload.filters)
      commit('setResults', results.data)
      commit('setTotalItems', results.totalItems)
    } catch (err) {
      console.log(err)
      // error handling
    }
    commit('setLoading', false);
  },
  setSelected ({ commit, state }, payload: { id: string, doAdd?: boolean }) {
    let newSelected = [...state.selected];
    if (payload.doAdd) {
      if (newSelected.indexOf(payload.id) === -1) {
        newSelected.push(payload.id);
      } else {
        newSelected.splice(newSelected.indexOf(payload.id), 1);
      }
    } else {
      newSelected = [payload.id];
    }
    commit('setSelected', newSelected);
  },
};

export default actions;
